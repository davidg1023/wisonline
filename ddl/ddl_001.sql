--
-- sql is the commands being triggered
-- s01, s02... is the replicationdatetime
--

CREATE TABLE `0_sync_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src_site_code` varchar(30) DEFAULT NULL,
  `sql_commands` text DEFAULT NULL,
  `sql_specific` text DEFAULT NULL,
  `s01` datetime DEFAULT NULL,
  `s02` datetime DEFAULT NULL,
  `s03` datetime DEFAULT NULL,
  `s04` datetime DEFAULT NULL,
  `s05` datetime DEFAULT NULL,
  `s06` datetime DEFAULT NULL,
  `s07` datetime DEFAULT NULL,
  `s08` datetime DEFAULT NULL,
  `s09` datetime DEFAULT NULL,
  `s10` datetime DEFAULT NULL,
  `s11` datetime DEFAULT NULL,
  `s12` datetime DEFAULT NULL,
  `s13` datetime DEFAULT NULL,
  `s14` datetime DEFAULT NULL,
  `s15` datetime DEFAULT NULL,
  `s16` datetime DEFAULT NULL,
  `s17` datetime DEFAULT NULL,
  `s18` datetime DEFAULT NULL,
  `s19` datetime DEFAULT NULL,
  `s20` datetime DEFAULT NULL,
  `timestampdetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
