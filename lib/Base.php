<?php

/**
* this is a global class for functions
*/

class Base {

	
	function connect_db($host, $username, $password, $dbname) {

		$mysql = new mysqli($host, $username, $password, $dbname) or die( mysql_error());

		!is_object($mysql) && $mysql = (object) $mysql;

		return $mysql;
	}

	function exec_sql($sql, $conn) {

		$ret = FALSE;

		// Check connection
		if ($conn->connect_error) {

		    $ret = FALSE;
		} 

		if ($conn->query($sql) === TRUE) {
		 
		 	$ret = TRUE;
		} else {
		 
		    $ret = FALSE;
		}

		$conn->close();

		return $ret;
	}

	function fetch_sql($sql, $conn, $field = "") {

		// Check connection
		if ($conn->connect_error) {
		
			return FALSE;
		} 

		$result = $conn->query($sql);

		$ret = array();

		if ($result->num_rows > 0) {
			
			if (empty($field)) {

				while($row = $result->fetch_assoc()) {
		        
			        $ret[] = $row;
			    }	
			}
			else if ($row = $result->fetch_assoc()) {

				$ret = $row[$field];
			}
		    
		}

		$conn->close();

		return $ret;
	}
}	

?>