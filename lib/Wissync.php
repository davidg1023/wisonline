<?php

//namespace lib;

require_once('Base.php');

class Wissync extends Base {

	protected $source = null;
	protected $targets = null;
	protected $tables = null;

	function __construct() {

	   require_once('config/config.php');

	   $this->source = $config['source'];
	   $this->targets = $config['targets'];
	   $this->tables = $config['tables'];
    }

    function sync_start() {

    	set_time_limit(0);

		try {
			
			# A. TARGET TO SOURCE

			foreach ($this->targets as $target) {

				$target = (object) $target;

		        if (empty($target->is_active)) {

		            continue;
		        }

		        # 1.0 CONNECT TO SOURCE
		        $source = (object) $this->source;
		        $db_source = $this->connect_db($source->source_host, $source->source_username, $source->source_password, $source->source_dbname);

		        /* check connection */
		        if ($db_source->connect_errno) {
		            
		            __log("Connect failed: %s\n", $db_source->connect_error);
		            exit;
		        }

		        # 2.0 CONNECT TO TARGET

				$db_target = $this->connect_db($target->target_host, $target->target_username, $target->target_password, $target->target_db);

				/* check connection */
				if ($db_target->connect_errno) {
				    
				    __log("Connect failed: %s\n", $db_target->connect_error);
			    	continue;
				}

		        # 3.0 SYNC
		        __logf("Synchronizing {$target->target_host} : {$target->target_db} => {$source->source_host} : {$source->source_dbname}");
		        $this->sync_data($db_target, $db_source,  $target->site_code, FALSE);

		        # 4.0 SYNC 
		        
		        __logf("Synchronizing {$source->source_host} : {$source->source_dbname} => {$target->target_host} : {$target->target_db}");
		        $this->sync_data($db_source, $db_target, $target->site_code, TRUE);

				//close target
				$db_target->close();

		        //close source
		        $db_source->close();
			}


			//end	
		} catch(Exception $e) {

			__logf("Caught Exception Error: %s\n", $e->getMessage());
			exit; 
		}
    }

	function sync_data($db_source, $db_target, $site_code, $is_source_host = FALSE) {

		//echo '<br>'.date("H:i:s A");

	    $newLine = "\r\n";
	    $output = $update = "";
	    $sync_id = 0;

	    if ($is_source_host) {

	        $scol = "s{$site_code}";
	        
	        $sql = "SELECT * FROM 0_sync_status WHERE {$scol} is null or {$scol} = '' LIMIT 500" ;
	        $rows = $db_source->query($sql);
	            
	        if ( $rows->num_rows > 0 ) { // where is_sync = 0
	            
	            while($row = $rows->fetch_assoc()) {

	                $sync_id = $row['id'];

	                $sql_commands = str_replace(";;", "||{$sync_id} ;; ", $row['sql_commands']);

	                $output .= $sql_commands;
	            }
	        }

	    }
	    else {

	        foreach($this->tables as $table_name => $columns ){

	            $col_check = "SHOW COLUMNS FROM `{$table_name}` LIKE 'ReplicationDateTime'";
	            
	            $col_exists = $db_source->query($col_check);
	            
	            if ( $col_exists->num_rows > 0 ) { // is_sync column exists for this table 
	                
	                $sql = "SELECT * FROM {$table_name} WHERE ReplicationDateTime is null or ReplicationDateTime = '' LIMIT 500" ;
	                
	                $rows = $db_source->query($sql);
	                
	                if ( $rows->num_rows > 0 ) { // where is_sync = 0

	                    $i = 0;

	                    while($row = $rows->fetch_assoc()) {
	                  
	                        $col_val = $update_col_val = $already_exists = array();
	                        
	                        $ts_now = __get_date_now();

	                        $has_site_code = FALSE;

	                        foreach($row as $name => $val) {

	                            if ( is_null( $val ) ) {
	                            
	                                continue;
	                            }
	                            
	                            $val = mysql_real_escape_string($val);

	                            if (!__is_blank($val) && !in_array(strtolower($name), array('timestampheader', 'timestampdetail'))) {

	                                $update_col_val[] = " `{$name}` = '{$val}' ";
	                            }

	                            if ( "ReplicationDateTime" === $name ) {
	                                
	                                $val = __get_date_now();
	                            }

	                            if ( "SiteCode" === $name ) {
	                                
	                                $val = $site_code;

	                                $has_site_code = TRUE;
	                            }

	                            $col_val[] = " `{$name}` = '{$val}' ";

	                            if ( strtolower($name) == strtolower($columns[0])){
	                                $already_exists[] = " `$columns[0]` = '{$val}'";
	                            }

	                            if ( isset($columns[1]) && strtolower($name) == strtolower($columns[1])){
	                                $already_exists[] = " `$columns[1]` = '{$val}'";
	                            }
	                        }

	                        if ( is_array($col_val) && count($col_val) > 0 ) {

	                            //echo $this->table_keys[$table_name];
	                            $filter_site_code = "";

	                            // if ($has_site_code) {

	                            //     $filter_site_code = "SiteCode = '{$site_code}' AND ";
	                            // }

	                            $output .= "SELECT * FROM `{$table_name}` WHERE {$filter_site_code}" . implode(" AND ", $already_exists) . " ||";
	                            $output .= "INSERT INTO `{$table_name}` SET " . implode(",", $col_val) . " ||";
	                            $output .= "UPDATE `{$table_name}` SET " . implode(",", $col_val) . " WHERE {$filter_site_code} " . implode(" AND ", $already_exists);
	                            $output .= " ||";

	                            $output .= "UPDATE {$table_name} SET ReplicationDateTime = '".$ts_now."' WHERE {$filter_site_code}" . implode(" AND ", $already_exists) . " ;; ";

	                            //__log($output);

	                            //__loge($update);

	                            //break at 10 records
	                            
	                            $i++;

	                            if ($i >= SYNC_PROCESS_MAX_LIMIT) {

	                                break;
	                            }
	                        }
	                    } //while
	                } // if is_sync = 0

	            } // if is_sync column exists 
	        }
	    }

	    if ( !empty($output) && $this->post_data($output, $db_source, $db_target, $site_code, $is_source_host)) {
	        // $update = explode(" ;; ", $update);
	        // foreach($update as $upq){
	        //     if (!empty($upq) ) {
	               
	        //         $db_target->query($upq);

	        //         __logf($upq);
	        //     }
	        // }
	    } else {
	        //echo $result;
	    }


	    //echo date("H:i:s A");
	}

	function post_data($output, $db_source, $db_target, $site_code, $is_source_host) {

	    try {

	    	if (!empty($output) ) {

	            $qry = $output;//htmlspecialchars_decode( urldecode( $_POST['qry']));

	            //$this->connect_local_db();

	            $qry = explode(" ;; ", $qry);

	            foreach($qry as $qr) {

	                $q = explode( "||", $qr );

	                if (empty($q[0])) {
	                   continue;
	                }

	                $exists = $db_target->query($q[0]);

	                if ( $exists && $exists->num_rows > 0 ) {

	                    $db_target->query( $q[2]) ;
	                     __logf($q[2]);

	                } else if (isset($q[1])) {

	                    $db_target->query($q[1]);
	                     __logf($q[1]);
	                }

	                isset($q[3]) && $db_source->query($q[3]);

	                if (!empty($is_source_host)) {
	                    
	                    $sync_id = isset($q[4]) ? $q[4] : 0;
	                    $this->update_sync_status($db_source, $site_code, $sync_id);
	                }
	                else {

	                    $this->insert_sync_status($db_target, $site_code, $output);
	                }

	               // __logf($q[3]);
	            }



	            return TRUE;
	        } else {
	            
	            //echo "qry not found";
	            __logf('query not found.');

	            return FALSE;
	        }

	    } catch(Exception $e) {

	        __logf("Caught Exception Error: %s\n", $e->getMessage());

	        return FALSE;
	    }

	    return TRUE;
	}

	function insert_sync_status($db_source, $site_code, $sql_commands, $sql_specific = '') {

		__logf('Inserting status');

	    $scol = "s{$site_code}";

	    $ts = __get_date_now();

	    $sql = "INSERT INTO 0_sync_status(src_site_code, sql_commands, sql_specific, {$scol})
	                    VALUES(\"{$site_code}\", \"{$sql_commands}\", \"{$sql_specific}\", \"{$ts}\")";
		__logf($sql);
	    $db_source->query($sql);
	}

	function update_sync_status($db_source, $site_code, $sync_id) {

		__logf('Updating status');

	    $scol = "s{$site_code}";

	    $ts = __get_date_now();

	    $sql = "UPDATE 0_sync_status set {$scol}='{$ts}' where id={$sync_id}";
		__logf($sql);
	    $db_source->query($sql);
	}
}

?>