<?php
/**
* Helpers are useful for global functions
*/

	function __get_date_now() {

		$mysqlDateTime = date('c');
		return date('Y-m-d H:i:s', strtotime($mysqlDateTime)); //2015-07-27T00:00:00+02:00
	}

	function __log($msg, $param = "") {

		 //printf("Connect failed: %s\n", $db_target->connect_error);
		 echo "<pre>";
		 print_r($msg, $param);
		 echo "</pre>";
	}

	function __loge($msg, $param = "") {

		 __log($msg, $param);
		 exit;
	}

	function __logf($msg, $param = "") {

		__log($msg, $param);

		$ts = date('Ymd');

		// if ($filesize > 10) {

		// }

		file_put_contents("logs/info_{$ts}.log", $msg . PHP_EOL, FILE_APPEND);
	}

	function __is_blank($val) {

		return empty($val) && !is_numeric($val);
	}

	function __get_file_size($file) {

		$filesize = 0;

		try {

			$filesize = filesize($file); // bytes
			$filesize = round($filesize / 1024 / 1024, 1); // megabytes with 1 digit

		} catch(Exception $e) {

			__logf($e->getMessage());
		}

		//echo "The size of your file is $filesize MB.";
		return $filesize;
	}
?>