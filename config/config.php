<?php

# Setup Source Host
$config['source']	= array(
		'source_host'       => 'localhost',
		'source_username'   => 'root',
		'source_password'   => '',
		'source_dbname'     => 'wis_db2',
		'source_site_code'  => '01'
	);

# Setup Target Host
$config['targets'] = array(
		array(
			'target_host'       => 'localhost',
			'target_username'   => 'root',
			'target_password'   => '',
			'target_db'   		=> 'wis_db3',
			'site_code' 		=> '04',
			'is_active'	 		=> 1
		),
		array(
			'target_host'       => 'localhost',
			'target_username'   => 'root',
			'target_password'   => '',
			'target_db'   		=> 'wis_db4',
			'site_code' 		=> '06',
			'is_active'			=> 1
		)
	);

# Setup Tables to Sync
$config['tables'] =  array(
	/*"0_system_version"=> "VersionNo",
	//"000_bank"=> "Bank",
	//"000_chart_of_account"=> "COA_CODE",
	"000_companies"=> "CompanyID",*/
	"100_dr_out" => array("DROutNo"),
	"100_dr_out_sf1" => array("DroutNo","LineID"),
	/*"000_customer"=> array("CustomerCode"),
	// "000_employee"=> "EmployeeCode",
	// "000_fiscal_years"=> "FiscalID",
	// "000_outlets"=> "OutletCode",
	// "000_outlets_sf" => ["OutletCode", "CompanyID"],
	"000_product"=> array("ProductCode"),
	//"000_product_barcode_data"=> "ProductCode",
	"000_product_category"=> array("category"),
	"000_product_classification" => array("classification"),
	"000_product_conversion" => array("ProductCode", "LineID"),
	"000_product_sf_locator" => array("ProductCode", "LineID"),
	// "000_repairs",
	// "000_repairs_payment",
	// "000_repairs_payment_sf",
	// "000_repairs_releasing",
	// "000_repairs_releasing_sf",
	// "000_repairs_replacement",
	// "000_repairs_replacement_details",
	// "000_repairs_sf",
	// "000_replacement_services",
	// "000_salesperson" => "SPCode",
	// "000_section" => "SectionCode",
	// "000_sites" => "SiteCode",
	"000_supplier" => array("SupplierCode"),
	//"000_terms" => ,
	//"000_units",
	// "000_user" => "UserName",
	// "000_user_companies" => ["UserName", "CompanyID"],
	// "000_user_forms" => "CompanyID, UserName, FormName",
	//"000_warehouses" => "WarehouseCode",
	// "100_cash_invoice" => "CashInvNo",
	// "100_cash_invoice_sf1" => "CashInvNo, ProductCode",
	// "000_pos_payments" => 
	// "100_co_bank" => 
	// "100_consignment_customer" => 
	// "100_consignment_customer_sf" => 
	// "100_consignment_supplier" => ,
	// "100_consignment_supplier_sf" => ,
	// "100_cr_memo" => ,
	// "100_cr_memo_sf1" => ,
	// "100_cv" => ,
	// "100_cv_sf1" => ,
	// "100_cv_sf2_drin" => ,
	"100_dr_in" => array("DrinNo"),
	"100_dr_in_sf1" => array("DrinNo","LineID"),
	// "100_dr_memo" => ,
	// "100_dr_memo_sf1" => ,
	//"100_dr_out" => array("DroutNo"),
	"100_dr_out_sf1" => array("DroutNo","LineID"),
	// "100_general_ledger" => ,
	// "100_general_ledger_sf" => ,
	// "100_invoice_cash" => ,
	// "100_invoice_cash_sf" => ,
	// "100_jv" => ,
	// "100_jv_sf" => ,
	// "100_or" => ,
	// "100_or_sf1" => ,
	// "100_pdc_check" => ,
	"100_po" => array("PoNo"),
	"100_po_sf1" => array("PoNo","LineID"),
	// "100_production" => ,
	// "100_production_sf_input" => ,
	// "100_production_sf_output" => ,
	// "100_quotation" => ,
	// "100_quotation_sf1" => ,
	// "100_releasing" => ,
	// "100_releasing_sf" => ,
	"100_sales_invoice" => array("INVNo"),
	// "100_sales_order" => ,
	// "100_sales_order_sf1" => ,
	// "100_sop" => ,
	// "100_sop_sf" => ,
	"100_stock_adj" => array("StockAdjNo"),
	"100_stock_adj_sf" => array("StockAdjNo","ProductCode"),
	// "100_stock_in" => ,
	// "100_stock_in_sf" => ,
	"100_stock_transfer" => array("STNo"),
	"100_stock_transfer_sf" => array("STNo","LineID"),
	// "200_aging_beg_ap" => ,
	// "200_aging_beg_ar" => ,
	// "200_cash_beg" => ,
	// "200_credit_memo" => ,
	// "200_credit_memo_entry" => ,
	// "200_debit_memo" => ,
	// "200_debit_memo_entry" => ,
	// "200_fs_bs" => ,
	// "200_fs_bs_beginning" => ,
	// "200_fs_bs_current" => ,
	// "200_fs_is" => ,
	// "200_fs_is_beginning" => ,
	// "200_fs_is_current" => ,
	// "200_fs_tb_details" => ,
	// "300_bunos_card" => ,
	// "300_bunos_card_sf"*/
);

?>