<?php
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	require_once('config/constants.php');
	require_once('config/helpers.php');
	
	require_once('lib/Wissync.php');

	$sync = new Wissync();

	$sync->sync_start();
?>